// ThankYouPage.js
import React from "react";

function ThankYouPage() {
  return (
    <div>
      <h1>Thank You for Your Message!</h1>
      {/* Your content for the thank you page */}
      <p>Your message has been successfully received.</p>
      <p>We appreciate your interest and will respond as soon as possible.</p>
      <p>In the meantime, feel free to explore more of our website.</p>
    </div>
  );
}

export default ThankYouPage;
