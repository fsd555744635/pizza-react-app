import React from "react";
import MultiplePizzas from "../assets/multiplePizzas.jpeg";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import "../styles/Footer.css";
import "../styles/About.css";
function About() {
  return (
    <div className="about">
      <div
        className="aboutTop"
        style={{ backgroundImage: `url(${MultiplePizzas})` }}
      ></div>
      <div className="aboutBottom">
        <h1> ABOUT US</h1>
        <p>
        At Karthik's Pizzeria, we're passionate about crafting the finest pizzas to satisfy every taste bud.
        
        Our journey began with a vision to deliver mouthwatering pizzas made from fresh ingredients and 
        authentic recipes.Karthik's Pizzeria is a celebration of tradition, quality, and
        innovation. Every pizza that leaves our kitchen reflects our commitment to using the
        freshest ingredients and preserving the authentic taste.Our journey at Karthik's 
        Pizzeria began with a vision — to redefine the art of pizza making. From our humble beginnings,
        we've embarked on a flavorful adventure,
        dedicated to crafting pizzas that tantalize your taste buds and warm your heart.
        </p>
        <p>
        Join us in our journey as we continue to deliver not just pizzas, but unforgettable
        experiences that bring people together around the love for great food.  
        </p>
        <p>
        Follow us on social media to stay updated on our latest flavors, offers, and behind-the-scenes
        glimpses of our kitchen:
        </p>
        <p>Connect with us on social media and become a part of our pizza-loving community!</p>
        <div className="socialMedia">
        <a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer">
          <InstagramIcon />
        </a>
        <a href="https://www.linkedin.com/in/karthik-reddy-1a74aa28a" target="_blank" rel="noopener noreferrer">
          <LinkedInIcon />
        </a>
        <a href="https://twitter.com/home" target="_blank" rel="noopener noreferrer">
          <TwitterIcon />
        </a>
        <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
          <FacebookIcon />
        </a>
      </div>
      </div>
    </div>
  );
}

export default About;