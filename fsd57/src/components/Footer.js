  import React from "react";
  import "../styles/Footer.css";
  import InstagramIcon from "@material-ui/icons/Instagram";
  import TwitterIcon from "@material-ui/icons/Twitter";
  import FacebookIcon from "@material-ui/icons/Facebook";
  import LinkedInIcon from "@material-ui/icons/LinkedIn";

  function Footer() {
    return (
      <div className="footer">
        <div className="socialMedia">
          <a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer">
            <InstagramIcon />
          </a>
          <a href="https://www.linkedin.com/in/karthik-reddy-1a74aa28a" target="_blank" rel="noopener noreferrer">
            <LinkedInIcon />
          </a>
          <a href="https://twitter.com/home" target="_blank" rel="noopener noreferrer">
            <TwitterIcon />
          </a>
          <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
            <FacebookIcon />
          </a>
        </div>
        <p> &copy; 2023 pedrotechpizza.com</p>
      </div>
    );
  }
  
  export default Footer;